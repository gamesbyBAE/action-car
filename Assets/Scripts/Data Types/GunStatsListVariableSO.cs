using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This class is used to create a SO to be used as a list data container.
/// </summary>
[CreateAssetMenu(fileName = "New Gun Stats List Value", menuName = "Data Types/Gun Stats List Value")]
public class GunStatsListVariableSO : ScriptableObject
{
    [SerializeField] List<GunStats> gunStatsList;
    public List<GunStats> GunStatsList() => gunStatsList;
}
