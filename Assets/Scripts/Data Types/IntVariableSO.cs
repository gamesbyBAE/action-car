using UnityEngine;

/// <summary>
/// This class is used to create a SO to be used as a string data container.
/// </summary>
[CreateAssetMenu(fileName = "New Int Value", menuName = "Data Types/Int Value")]
public class IntVariableSO : ScriptableObject
{
    [SerializeField] int intValue;

    public int IntValue() => intValue;
}
