using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This class is used to create a SO to be used as a list data container.
/// </summary>
[CreateAssetMenu(fileName = "New Game Object List Value", menuName = "Data Types/GO List Value")]
public class GOListVariableSO : ScriptableObject
{
    [SerializeField] List<GameObject> gameObjectList;

    public List<GameObject> GameObjectList() => gameObjectList;
}
