using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used to raise & listen to Events that has one bool argument.
/// </summary>
[CreateAssetMenu(fileName = "New Bool Event Channel", menuName = "Events/Bool Event Channel")]
public class BoolEventChannelSO : ScriptableObject
{
    public UnityAction<bool> OnBoolValueRaised;

    public void RaiseEvent(bool val)
    {
        OnBoolValueRaised?.Invoke(val);
    }
}
