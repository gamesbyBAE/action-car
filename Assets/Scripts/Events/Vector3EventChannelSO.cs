using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used to raise & listen to Events that has one Vector3 argument.
/// </summary>
[CreateAssetMenu(fileName = "New Vector3 Event Channel", menuName = "Events/Vector3 Event Channel")]
public class Vector3EventChannelSO : ScriptableObject
{
    public UnityAction<Vector3> OnVector3ValueRaised;

    public void RaiseEvent(Vector3 val)
    {
        OnVector3ValueRaised?.Invoke(val);
    }
}
