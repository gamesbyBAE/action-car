using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used to raise & listen to Events that has one GameObject argument.
/// </summary>
[CreateAssetMenu(fileName = "New GameObject Event Channel", menuName = "Events/GameObject Event Channel")]
public class GameObjectEventChannelSO : ScriptableObject
{
    public UnityAction<GameObject> OnGameObjectValueRaised;

    public void RaiseEvent(GameObject go)
    {
        OnGameObjectValueRaised?.Invoke(go);
    }
}
