using UnityEngine;

public class ExplosionSpawner : MonoBehaviour
{
    [Header("Events to Subscribe")]
    [SerializeField] Vector3EventChannelSO spawnPositionEvent = default;

    Pooler explosionPool;
    private void OnEnable()
    {
        spawnPositionEvent.OnVector3ValueRaised += SpawnExplosion;
    }
    private void OnDisable ()
    {
        spawnPositionEvent.OnVector3ValueRaised -= SpawnExplosion;
    }

    private void Start()
    {
        explosionPool = GetComponent<Pooler>();
    }

    void SpawnExplosion(Vector3 pos)
    {
        GameObject go = explosionPool.GetFromPool();
        go.transform.position = pos;
        go.SetActive(true);
    }
}
