using System.Collections;
using UnityEngine;

public class DisableExplosion : MonoBehaviour
{
    [Header("Events to Raise")]
    [SerializeField] GameObjectEventChannelSO usedExplosionGO = default;
    [SerializeField] float disableTime = default;

    IEnumerator coroutine;
    
    private void OnEnable()
    {
        coroutine = DisableGameObject();
        StartCoroutine(coroutine);
    }
    IEnumerator DisableGameObject()
    {
        yield return new WaitForSeconds(disableTime);
        usedExplosionGO.RaiseEvent(this.gameObject);
    }
}
