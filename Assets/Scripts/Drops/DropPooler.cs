using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pools different drops, takes out from as well as
/// puts them back in their respective pools.
/// </summary>
public class DropPooler : MonoBehaviour
{
    [SerializeField] int eachDropCount = 5;
    [SerializeField] GameObject[] dropPrefabsArray = new GameObject[4];

    [Header("Events to Raise")]
    [SerializeField] GameObjectEventChannelSO newDropGO = default;

    [Header("Events to Subscribe")]
    [SerializeField] VoidEventChannelSO getFromPoolEvent = default;
    [SerializeField] GameObjectEventChannelSO usedDropGO = default;

    Dictionary<string, int> dropMap;
    Queue<GameObject>[] poolQueue;

    private void Start()
    {
        poolQueue = new Queue<GameObject>[dropPrefabsArray.Length];
        dropMap = new Dictionary<string, int>();

        for (int i = 0; i < dropPrefabsArray.Length; i++)
        {
            dropMap[dropPrefabsArray[i].transform.tag] = i; //Initialising Dictionary

            poolQueue[i] = new Queue<GameObject>();
            for (int j = 0; j < eachDropCount; j++)
            {
                GameObject dropGO = Instantiate(dropPrefabsArray[i], transform);
                dropGO.SetActive(false);
                poolQueue[i].Enqueue(dropGO);
            }
        }
    }

    private void OnEnable()
    {
        getFromPoolEvent.OnVoidEventRaised += GetFromPool;
        usedDropGO.OnGameObjectValueRaised += PutInPool;
    }
    private void OnDisable()
    {
        getFromPoolEvent.OnVoidEventRaised -= GetFromPool;
        usedDropGO.OnGameObjectValueRaised -= PutInPool;
    }

    void GetFromPool()
    {
        int rand = Random.Range(0, dropPrefabsArray.Length-1);

        GameObject nextGO = poolQueue[rand].Count > 0 ? poolQueue[rand].Dequeue() : Instantiate(dropPrefabsArray[rand], transform);
        nextGO.SetActive(true);
        newDropGO.RaiseEvent(nextGO);
    }
    void PutInPool(GameObject usedBullet)
    {
        usedBullet.SetActive(false);
        dropMap.TryGetValue(usedBullet.transform.tag,out int queueIndex);
        poolQueue[queueIndex].Enqueue(usedBullet);
    }
}
