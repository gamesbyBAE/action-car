using UnityEngine;

/// <summary>
/// Disables the Drop game object when comes in contact
/// with the Player game object and raises event with it's value.
/// </summary>
public class DropBehaviour : MonoBehaviour
{
    [SerializeField] int dropValue = default;
    [SerializeField] StringVariableSO playerTag = default;

    [Header("Events to Raise")]
    [SerializeField] GameObjectEventChannelSO usedAbility = default; //To put it back in the pool.
    [SerializeField] IntEventChannelSO pickedDropValue = default; //Raise event with it's value as argument.

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag.StringValue()))
        {
            pickedDropValue.RaiseEvent(dropValue);
            usedAbility.RaiseEvent(this.gameObject);
        }
    }
}
