using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Spawns crates in empty places at regular intervals.
/// When crates are destroyed, places the drops in that position.
/// </summary>
public class DropsSpawner : MonoBehaviour
{
    [SerializeField] Pooler cratesPool = default;

    // Spawning
    [Header("Spawning")]
    [Tooltip("Maximum no of drops that should be allowed on the arena at once.")]
    [SerializeField] int maxDropsActive = 9;

    [Tooltip("Time interval after which to spawn a new ability.")]
    [SerializeField] float timeInterval = 8;
    WaitForSeconds waitTime;
    WaitUntil waitUntilReadyToSpawn;

    // Spawn Region
    [Header("Spawn Region")]
    [SerializeField] Collider groundGameObject = default;

    [Tooltip("x-value to subtract from ground's width(total x-distance), to avoid spawning at edges of the ground.")]
    [SerializeField] float xOffset = default;

    [Tooltip("z-value to subtract from ground's width(total z-distance), to avoid spawning at edges of the ground.")]
    [SerializeField] float zOffset = default;
    float xPosMin, xPosMax;
    float zPosMin, zPosMax;

    [Tooltip("Radius to create a sphere which checks for empty space suitable for spawning.")]
    [SerializeField] float spawnPositionCheckerRadius = default;

    [Tooltip("Layer which the Spawn Position Checker checks for collision with.")]
    [SerializeField] LayerMask collisionLayerMask = default;

    [Tooltip("No. of times loop checks for new position, in case it overlaps with other objects to avoid being stuck in an infinite loop.")]
    [SerializeField] int maxPositionCheckerQuery = default;

    // Events
    [Header("Events to Subscribe")]
    [SerializeField] Vector2EventChannelSO cratePos = default;
    [SerializeField] GameObjectEventChannelSO newDropGO = default;
    [SerializeField] GameObjectEventChannelSO usedDropGO = default;

    [Header("Events to Raise")]
    [SerializeField] VoidEventChannelSO getDropGO = default;

    Vector2 newDropPos;
    int activeDropsCount;
    IEnumerator spawnCrateCoroutine;

    Vector3 newPosition;

    private void Start()
    {
        activeDropsCount = 0;
        newDropPos = Vector2.zero;

        waitTime = new WaitForSeconds(timeInterval);

        xPosMin = groundGameObject.bounds.min.x + xOffset;
        xPosMax = groundGameObject.bounds.max.x - xOffset;
        zPosMin = groundGameObject.bounds.min.z + zOffset;
        zPosMax = groundGameObject.bounds.max.z - zOffset;

        spawnCrateCoroutine = SpawnCrateCountdown();
        StartCoroutine(spawnCrateCoroutine);
    }
    private void OnEnable()
    {
        cratePos.OnVector2ValueRaised += StoreCratePos;
        newDropGO.OnGameObjectValueRaised += SpawnDrop;
        usedDropGO.OnGameObjectValueRaised += DecreaseActiveDropCount;
    }
    private void OnDisable()
    {
        cratePos.OnVector2ValueRaised -= StoreCratePos;
        newDropGO.OnGameObjectValueRaised -= SpawnDrop;
        usedDropGO.OnGameObjectValueRaised -= DecreaseActiveDropCount;

        StopCoroutine(spawnCrateCoroutine);
    }

    IEnumerator SpawnCrateCountdown()
    {
        yield return new WaitForSeconds(2); //Starts spawning after few seconds.

        while (true)
        {
            yield return waitTime;

            if (activeDropsCount < maxDropsActive)
            {
                Vector3 newSpawnPos = GetSpawnPosition();

                if (newSpawnPos != Vector3.zero)
                {
                    GameObject crateGO = cratesPool.GetFromPool();
                    crateGO.transform.position = newSpawnPos;
                    activeDropsCount += 1;
                }
            }
        }
    }

    private Vector3 GetSpawnPosition()
    {
        int count = 0;
        while (count < maxPositionCheckerQuery)
        {
            float xPos = Random.Range(xPosMin, xPosMax);
            float zPos = Random.Range(zPosMin, zPosMax);
            newPosition = new Vector3(xPos, 0, zPos); //Can make it local if OnDrawGizmos is not being used.

            // Checks if any other GOs on layer, 'Default' collide/overlap with the sphere of radius 'spawnPositionCheckerRadius'
            // at the new randomly generated position, 'newPosition'.
            Collider[] collisionCheck = new Collider[1];
            int overlapCount = Physics.OverlapSphereNonAlloc(newPosition, spawnPositionCheckerRadius, collisionCheck, collisionLayerMask); // Generates no garbage unlike OverlapSphere.
            if ((overlapCount == 0) && (newPosition != Vector3.zero))
            {
                return newPosition;
            }

            count += 1;
        }
        return Vector3.zero;
    }

    //Stores Crate's position when it has been destroyed by the player.
    //Raises an event to get a new Drop game object.
    void StoreCratePos(Vector2 val)
    {
        newDropPos = val;
        getDropGO.RaiseEvent();
    }

    //Updates the position of the received Drop game object.
    void SpawnDrop(GameObject go)
    {
        go.transform.position = new Vector3(newDropPos.x, 0, newDropPos.y);
    }

    // Decrements the counter when the Drop has been picked up by the Player.
    void DecreaseActiveDropCount(GameObject go)
    {
        activeDropsCount -= 1;
        if (activeDropsCount < 0)
        {
            activeDropsCount = 0;
        }
    }

    //To visualise the sphere created to check if the space is empty in 
    //GetSpawnPosition method.
    //To make it work declare 'newPosition' globally rather than locally
    //in Line-116.
    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(newPosition, spawnPositionCheckerRadius);
    }*/
}
