using UnityEngine;

public class CrateBehaviour : MonoBehaviour
{
    [SerializeField] StringVariableSO playerBulletTag = default;

    [Header("Events to Raise")]
    [SerializeField] GameObjectEventChannelSO usedCrate = default; //Used to put it back in the pool.
    [SerializeField] Vector2EventChannelSO currentPos = default; //Sends it's position to DropsSpawner to spawn drops at this position.

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(playerBulletTag.StringValue()))
        {
            Vector2 pos = new Vector2(this.transform.position.x, this.transform.position.z);
            currentPos.RaiseEvent(pos);

            usedCrate.RaiseEvent(this.gameObject);
        }
    }
}
