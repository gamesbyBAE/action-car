using UnityEngine;
using TMPro;

/// <summary>
/// Updates the ammo count text of gun buttons.
/// </summary>
public class AmmoTracker : MonoBehaviour
{
    [SerializeField] GunStatsListVariableSO gunsList = default;
    [SerializeField] bool isGun1 = default; 

    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO ammoCount = default;

    TextMeshProUGUI ammoText;
    int gunIndex;

    private void Start()
    {
        ammoText = GetComponent<TextMeshProUGUI>();

        if (isGun1)
        {
            gunIndex = UserSelection.Instance.Gun1Index;
            ToTrack(true);
        }
        else
        {
            gunIndex = UserSelection.Instance.Gun2Index;
        }

        ammoText.SetText("{0}/{1}", gunsList.GunStatsList()[gunIndex].MaxAmmo, gunsList.GunStatsList()[gunIndex].MaxAmmo);

    }
    public void ToTrack(bool val)
    {
        if (val)
        {
            ammoCount.OnIntValueRaised += UpdateText;
        }
        else
        {
            ammoCount.OnIntValueRaised -= UpdateText;
        }
    }

    void UpdateText(int val)
    {
        ammoText.SetText("{0}/{1}", val, gunsList.GunStatsList()[gunIndex].MaxAmmo);
    }
}
