using UnityEngine;

public class GunEquipper : MonoBehaviour
{
    [SerializeField] GOListVariableSO carsList = default;
    [SerializeField] GunStatsListVariableSO gunsList = default;

    [Header("Events to Raise")]
    [SerializeField] IntEventChannelSO gunsListIndex = default;

    [Header("Events to Subcribe")]
    [SerializeField] IntEventChannelSO carsListIndex = default;
    [SerializeField] IntEventChannelSO useGunNumberEvent = default;

    GameObject gun1GO, gun2GO;

    void OnEnable()
    {
        carsListIndex.OnIntValueRaised += Position;
        useGunNumberEvent.OnIntValueRaised += SwitchGun;

        SpawnGuns();
    }
    private void OnDisable()
    {
        carsListIndex.OnIntValueRaised -= Position;
        useGunNumberEvent.OnIntValueRaised -= SwitchGun;
    }

    private void Start()
    {
        gunsListIndex.RaiseEvent(UserSelection.Instance.Gun1Index);
    }
    // Changes the position of this game object according to the mount position of respective cars.
    void Position(int val)
    {
        this.transform.localPosition = carsList.GameObjectList()[val].GetComponent<CarData>().GunMountPosition;
    }

    void SpawnGuns()
    {
        gun1GO = Instantiate(gunsList.GunStatsList()[UserSelection.Instance.Gun1Index].GunPrefab, transform);
        gun1GO.SetActive(true);

        gun2GO = Instantiate(gunsList.GunStatsList()[UserSelection.Instance.Gun2Index].GunPrefab, transform);
        gun2GO.SetActive(false);
    }

    void SwitchGun(int val)
    {
        if (val == 1)
        {
            gun1GO.SetActive(true);
            gun2GO.SetActive(false);
            gunsListIndex.RaiseEvent(UserSelection.Instance.Gun1Index);
        }
        else if (val == 2)
        {
            gun1GO.SetActive(false);
            gun2GO.SetActive(true);
            gunsListIndex.RaiseEvent(UserSelection.Instance.Gun2Index);
        }
    }
}
