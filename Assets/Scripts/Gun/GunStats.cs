using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun Stats SO", menuName = "Gun Stats")]
public class GunStats : ScriptableObject
{
    [SerializeField] GameObject gunPrefab = default;
    [SerializeField] int maxAmmo = default;

    [Tooltip("Number of bullets per minute")]
    [SerializeField] float fireRate = default;

    [SerializeField] int damage = default;

    [Range(0,100), Tooltip("Percentage bewteen 0 to 100")]
    [SerializeField] int critChance = default;
    [SerializeField] int critDamage = default;

    // Getters
    public GameObject GunPrefab => gunPrefab;
    public int MaxAmmo => maxAmmo;
    public float FireRate => fireRate;
    public int Damage => damage;
    public int CritChance => critChance;
    public int CritDamage => critDamage;
}
