using System.Collections;
using UnityEngine;

/// <summary>
/// Raises GameOver event when time limit has passed if
/// noLimit option has NOT been selected by the user.
/// </summary>
public class TimeTracker : MonoBehaviour
{
    [Header("Events to Raise")]
    [SerializeField] VoidEventChannelSO gameOverEvent = default;

    WaitForSeconds waitTime;
    IEnumerator coroutine;
    void Start()
    {
        if (UserSelection.Instance.TimeLimit > 0)
        {
            waitTime = new WaitForSeconds(UserSelection.Instance.TimeLimit);
            coroutine = Timer();
            StartCoroutine(coroutine);
        }
    }

    private void OnDisable()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
    }

    IEnumerator Timer()
    {
        yield return waitTime;
        gameOverEvent.RaiseEvent();
    }
}
