using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Detects input from user and raises corresponding events
/// for other subscribed classes.
/// </summary>
[CreateAssetMenu(fileName = "Input Reader", menuName = "Input Reader")]
public class InputReader : ScriptableObject, MyInputActions.IPlayerActionMapActions
{
    [Header("Events to Raise")]
    [SerializeField] Vector2EventChannelSO stickMovement = default;
    [SerializeField] BoolEventChannelSO isBreakingEvent = default;
    [SerializeField] BoolEventChannelSO isAcceleratingEvent = default;
    [SerializeField] BoolEventChannelSO isFiringBulletEvent = default;
    [SerializeField] BoolEventChannelSO isFiringMissileEvent = default;

    MyInputActions inputActions;
    private void OnEnable()
    {
        if (inputActions == null)
        {
            inputActions = new MyInputActions();

            inputActions.PlayerActionMap.SetCallbacks(this);
        }

        inputActions.Enable();
    }
    private void OnDisable()
    {
        inputActions.Disable();
    }

    public void OnMovement(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            stickMovement.RaiseEvent(inputActions.PlayerActionMap.Movement.ReadValue<Vector2>());
        }
        if (context.canceled)
        {
            stickMovement.RaiseEvent(inputActions.PlayerActionMap.Movement.ReadValue<Vector2>());
        }
    }

    public void OnAccelerate(InputAction.CallbackContext context)
    {

        if (context.phase == InputActionPhase.Performed)
        {
            isAcceleratingEvent.RaiseEvent(true);
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            isAcceleratingEvent.RaiseEvent(false);
        }
    }

    public void OnBrake(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            isBreakingEvent.RaiseEvent(true);
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            isBreakingEvent.RaiseEvent(false);
        }
    }

    public void OnFire(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            isFiringBulletEvent.RaiseEvent(true);
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            isFiringBulletEvent.RaiseEvent(false);
        }
    }

    public void OnMissile(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            isFiringMissileEvent.RaiseEvent(true);
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            isFiringMissileEvent.RaiseEvent(false);
        }
    }
}
