using UnityEngine;

/// <summary>
/// Handles accelerating, breaking and steering of the car.
/// </summary>
public class PlayerController : MonoBehaviour
{
    [SerializeField] GOListVariableSO carsList = default;

    [Header("Events to Raise")]
    [SerializeField] IntEventChannelSO carsListIndex = default;
    [SerializeField] IntEventChannelSO carMaxHPEvent = default;
    
    [Header("Events to Subscribe")]
    [SerializeField] Vector2EventChannelSO directionValueEvent = default;
    [SerializeField] BoolEventChannelSO isBreakingValueEvent = default;
    [SerializeField] BoolEventChannelSO isAcceleratingValueEvent = default;

    CarData carData;
    bool isBreaking, isAccelerating;
    Vector2 steerDirection;
    float currentSteerAngle;
    Rigidbody rb;
    BoxCollider boxCollider;

    private void OnEnable()
    {
        directionValueEvent.OnVector2ValueRaised += Direction;
        isAcceleratingValueEvent.OnBoolValueRaised += Accelerating;
        isBreakingValueEvent.OnBoolValueRaised += Breaking;
    }
    private void OnDisable()
    {
        directionValueEvent.OnVector2ValueRaised -= Direction;
        isAcceleratingValueEvent.OnBoolValueRaised -= Accelerating;
        isBreakingValueEvent.OnBoolValueRaised -= Breaking;
    }

    private void Start()
    {
        carsListIndex.RaiseEvent(UserSelection.Instance.CarIndex);
        InitialiseValues(UserSelection.Instance.CarIndex);
    }
    void InitialiseValues(int val)
    {
        GameObject carGO = Instantiate(carsList.GameObjectList()[val], transform);
        carData = carGO.GetComponent<CarData>();
        carGO.SetActive(true);
        carMaxHPEvent.RaiseEvent(carData.CarMaxHP);

        rb = GetComponent<Rigidbody>();
        rb.mass = carData.Mass;
        rb.centerOfMass = Vector3.zero;

        //Replicates the box collider of the car model
        //and disables from the car.
        boxCollider = GetComponent<BoxCollider>();
        boxCollider.size = carData.CarCollider.size;
        boxCollider.center = carData.CarCollider.center;
        carData.CarCollider.enabled = false;
    }

    private void Update()
    {
        UpdateVisuals();
    }

    private void FixedUpdate()
    {
        if (carData)
        {
            if ((isAccelerating) && (rb.velocity.z < carData.MaxSpeed))
            {
                MoveCar(carData.MotorForce);
            }
            else if (!isAccelerating)
            {
                MoveCar(0);
            }

            if (isBreaking)
            {
                ApplyBreak(carData.BreakForce);
                
                if (rb.velocity.z <= 0) // Reversing the car.
                {
                    ApplyBreak(0);
                    MoveCar(-carData.ReverseForce);
                }
            }
            else if (!isBreaking)
            {
                ApplyBreak(0);
            }

            Steering();
        }
    }
    
    void MoveCar(float motorForce)
    {
        carData.FrontLeftWC.motorTorque = motorForce;
        carData.FrontRightWC.motorTorque = motorForce;
        carData.BackLeftWC.motorTorque = motorForce;
        carData.BackRightWC.motorTorque = motorForce;
    }
    void ApplyBreak(float breakForce)
    {
        carData.FrontLeftWC.brakeTorque = breakForce;
        carData.FrontRightWC.brakeTorque = breakForce;
        carData.BackLeftWC.brakeTorque = breakForce;
        carData.BackRightWC.brakeTorque = breakForce;
    }

    void Steering()
    {
        currentSteerAngle = carData.MaxSteerAngle * Mathf.Atan2(steerDirection.x, steerDirection.y);
        carData.FrontLeftWC.steerAngle = currentSteerAngle;
        carData.FrontRightWC.steerAngle = currentSteerAngle;
    }

    // Rotates the wheel meshes and turns accordingly.
    void UpdateVisuals()
    {
        UpdateSingleWheel(carData.FrontLeftWC, carData.FrontLeftWheel);
        UpdateSingleWheel(carData.FrontRightWC, carData.FrontRightWheel);
        UpdateSingleWheel(carData.BackLeftWC, carData.BackLeftWheel);
        UpdateSingleWheel(carData.BackRightWC, carData.BackRightWheel);
    }
    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        wheelCollider.GetWorldPose(out Vector3 pos, out Quaternion rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }

    void Direction(Vector2 dir)
    {
        // Converting the negative value to positive since we don't want the tire
        // to overshoot maxSteerAngle & suddenly face opposite direction when
        // the joystick is pulled down.
        if (dir.y < 0)
        {
            dir.x = dir.x < 0 ? -1 : 1;
            dir.y = -dir.y;
        }

        steerDirection = dir;
    }

    void Accelerating(bool val)
    {
        isAccelerating = val;

        // Stop Accelerating the Car.
        /*if (!isAccelerating)
        {
            MoveCar(0);
        }*/
    }

    void Breaking(bool val)
    {
        isBreaking = val;
        /*if (!isBreaking)
        {
            MoveCar(0);
        }*/
    }
}
