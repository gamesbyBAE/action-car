using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    [SerializeField] Image fillImage = default;

    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO carMaxHP = default;
    [SerializeField] IntEventChannelSO currentHP = default;

    int maxHP;
    private void OnEnable()
    {
        carMaxHP.OnIntValueRaised += ReceiveMaxHP;
        currentHP.OnIntValueRaised += UpdateBar;
    }
    private void OnDisable()
    {
        carMaxHP.OnIntValueRaised -= ReceiveMaxHP;
        currentHP.OnIntValueRaised -= UpdateBar;
    }

    void ReceiveMaxHP(int val)
    {
        maxHP = val;
        fillImage.fillAmount = 1;
    }

    void UpdateBar(int val)
    {
        fillImage.fillAmount = (float) val / maxHP;
    }
}
