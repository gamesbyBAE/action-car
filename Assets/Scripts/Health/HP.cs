using UnityEngine;

public class HP : MonoBehaviour
{
    [SerializeField] VoidEventChannelSO informDeath = default;
    public int MaxHP { get; set; }
    
    int currentHP;
    public int CurrentHP => currentHP;

    public virtual void Start()
    {
        currentHP = MaxHP;
    }

    public virtual void ChangeHP(int val)
    {
        currentHP += val;
        if (currentHP > MaxHP)
        {
            currentHP = MaxHP;
        }
        else if (currentHP <= 0)
        {
            informDeath.RaiseEvent();
            this.gameObject.SetActive(false);
        }
    }
}
