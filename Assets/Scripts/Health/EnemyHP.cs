using UnityEngine;

public class EnemyHP : HP
{
    [SerializeField] IntVariableSO enemyMaxHP = default;

    public override void Start()
    {
        MaxHP = enemyMaxHP.IntValue();
        base.Start();
    }
}
