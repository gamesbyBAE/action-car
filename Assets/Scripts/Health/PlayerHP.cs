using UnityEngine;

public class PlayerHP : HP
{
    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO carMaxHP = default;
    [SerializeField] IntEventChannelSO pickedHPValueEvent = default;

    [Header("Events to Raise")]
    [SerializeField] IntEventChannelSO playerHPChanged = default;


    private void OnEnable()
    {
        carMaxHP.OnIntValueRaised += ReceivedIndex;
        pickedHPValueEvent.OnIntValueRaised += ChangeHP;
    }

    private void OnDisable()
    {
        carMaxHP.OnIntValueRaised = ReceivedIndex;
        pickedHPValueEvent.OnIntValueRaised += ChangeHP;
    }

    void ReceivedIndex(int val)
    {
        MaxHP = val;
        base.Start();
    }

    public override void ChangeHP(int val)
    {
        base.ChangeHP(val);
        playerHPChanged.RaiseEvent(CurrentHP);
    }
}
