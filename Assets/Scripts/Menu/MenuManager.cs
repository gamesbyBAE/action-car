using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu = default;
    [SerializeField] GameObject gameOverMenu = default;

    [Header("Events to Subscribe")]
    [SerializeField] VoidEventChannelSO gameOverEvent = default;
    [SerializeField] VoidEventChannelSO pausePressedEvent = default;

    Canvas canvas;
    bool isPaused;

    void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled=false;

        isPaused = false;

        pauseMenu.SetActive(false);
        gameOverMenu.SetActive(false);
    }

    private void OnEnable()
    {
        gameOverEvent.OnVoidEventRaised += ShowGameOver;
        pausePressedEvent.OnVoidEventRaised += PauseUnpause;
    }

    private void OnDisable()
    {
        gameOverEvent.OnVoidEventRaised -= ShowGameOver;
        pausePressedEvent.OnVoidEventRaised -= PauseUnpause;

    }
    void ShowGameOver()
    {
        canvas.enabled = true;
        gameOverMenu.SetActive(true);
        pauseMenu.SetActive(false);

        Time.timeScale = 0;
    }

    void PauseUnpause()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            ShowPauseMenu();
        }
        else
        {
            HidePauseMenu();
        }
    }
    void ShowPauseMenu()
    {
        canvas.enabled = true;
        pauseMenu.SetActive(true);
        gameOverMenu.SetActive(false);

        Time.timeScale = 0;
    }

    void HidePauseMenu()
    {
        pauseMenu.SetActive(false);
        canvas.enabled = false;

        Time.timeScale = 1;
    }
}
