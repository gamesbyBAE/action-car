using UnityEngine;
using TMPro;

/// <summary>
/// Updates the text values of kill counter in the Game Over Menu.
/// </summary>
public class UpdateKillCountText : MonoBehaviour
{
    [Header("Events to Raise")]
    [SerializeField] VoidEventChannelSO requestKillCountEvent = default;

    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO killCountValueEvent = default;
    [SerializeField] IntEventChannelSO totalTurretCountValueEvent = default;

    int killCount, totalCount;

    private void OnEnable()
    {
        killCountValueEvent.OnIntValueRaised += UpdateKillCount;
        totalTurretCountValueEvent.OnIntValueRaised += UpdateTotalCount;

        requestKillCountEvent.RaiseEvent();
    }
    private void OnDisable()
    {
        killCountValueEvent.OnIntValueRaised -= UpdateKillCount;   
        totalTurretCountValueEvent.OnIntValueRaised -= UpdateTotalCount;
    }

    void UpdateKillCount(int val)
    {
        killCount = val;
        UpdateText();
    }
    void UpdateTotalCount(int val)
    {
        totalCount = val;
        UpdateText();
    }
    void UpdateText()
    {
        GetComponent<TextMeshProUGUI>().SetText("{0}/{1}", killCount, totalCount);
    }
}
