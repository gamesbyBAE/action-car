using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inherits from Shoot class.
/// Handles shooting of Player.
/// </summary>
public class PlayerShoot : Shoot
{
    [Header("Events to Subscribe")]
    [SerializeField] BoolEventChannelSO isFiring = default;
    [SerializeField] IntEventChannelSO gunsListIndex = default;
    [SerializeField] IntEventChannelSO pickedAmmoValueEvent = default;

    Dictionary<int, int> ammoCountMap;
    int prevGunIndex = -1;
    private void OnEnable()
    {
        isFiring.OnBoolValueRaised += base.ToFire;
        gunsListIndex.OnIntValueRaised += Initialise;
        pickedAmmoValueEvent.OnIntValueRaised += AmmoRefill;
    }
    private void OnDisable()
    {
        isFiring.OnBoolValueRaised -= base.ToFire;
        gunsListIndex.OnIntValueRaised -= Initialise;
        pickedAmmoValueEvent.OnIntValueRaised -= AmmoRefill;
    }
    public override void Initialise(int val)
    {
        if (ammoCountMap == null)
        {
            ammoCountMap = new Dictionary<int, int>();
        }

        // Populating the Dictionary
        if(!ammoCountMap.ContainsKey(val))
        {
            ammoCountMap[val] = gunsList.GunStatsList()[val].MaxAmmo;
        }

        // Updates the value with latest ammo left in Dictionary on gun switch.
        if (prevGunIndex >= 0) //Guns list can't have negative index. Perhaps not running for the first time.
        {
            ammoCountMap[prevGunIndex] = ammoCount;
        }

        prevGunIndex = val;

        ammoCountMap.TryGetValue(val, out int ammoLeft);
        ammoCount = ammoLeft;
        
        base.Initialise(val);
    }

    void AmmoRefill(int val)
    {
        ammoCount += val;
        ammoCount = Mathf.Clamp(ammoCount, val, gunsList.GunStatsList()[prevGunIndex].MaxAmmo);
        base.ammoCountEvent.RaiseEvent(ammoCount);
    }
}
