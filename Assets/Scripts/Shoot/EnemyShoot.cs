using UnityEngine;

/// <summary>
/// Inherits from Shoot class.
/// Handles shooting of enemies.
/// </summary>
public class EnemyShoot : Shoot
{
    [Tooltip("Index of the Gun Stats to use from the list.")]
    [SerializeField] int gunListIndex = default;

    bool startFiring;
    private void Start()
    {
        StartFiring = false;

        Initialise(gunListIndex);
    }
    public override void Initialise(int val)
    {
        base.Initialise(val);
        ammoCount = 1;
    }

    public bool StartFiring
    { 
        get
        {
            return startFiring;
        }
        set
        {
            startFiring = value;
            base.ToFire(startFiring);
        }
    }
}
