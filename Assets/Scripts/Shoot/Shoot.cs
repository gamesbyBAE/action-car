using System.Collections;
using UnityEngine;

/// <summary>
/// Base class that handles shooting of bullets, assigning damage value
/// to the bullets as well as shooting the bullets from correct position.
/// </summary>
public class Shoot : MonoBehaviour
{
    [SerializeField] float projectileSpeed = default;
    [SerializeField] bool infiniteAmmo = false;
    [SerializeField] Pooler bulletsPool = default;
    [SerializeField] protected GunStatsListVariableSO gunsList = default;

    [SerializeField] protected IntEventChannelSO ammoCountEvent = default;


    IEnumerator coroutine;
    WaitForSeconds waitTime;

    GameObject projectileGO;
    Transform bulletSpawnPosition;
    int damageToApply, currentGunIndex;

    protected int ammoCount;
    public virtual void Initialise(int val)
    {
        currentGunIndex = val;

        bulletSpawnPosition = gunsList.GunStatsList()[currentGunIndex].GunPrefab.transform.GetChild(0).gameObject.transform;

        //Simple unitary method to convert given firerate(bullets per minute) to
        //after how many seconds of pause/suspension of the method next bullet can be fired.
        waitTime = new WaitForSeconds(60 / gunsList.GunStatsList()[currentGunIndex].FireRate);
    }
    protected void ToFire(bool val)
    {
        if (val == true)
        {
            coroutine = FireCoroutine();
            StartCoroutine(coroutine);
        }
        else
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }
        }
    }

    IEnumerator FireCoroutine()
    {
        while (true)
        {
            if (ammoCount > 0)
            {
                projectileGO = bulletsPool.GetFromPool();

                BulletDamageCalculator();

                projectileGO.transform.position = transform.TransformPoint(bulletSpawnPosition.localPosition);
                projectileGO.transform.localRotation = Quaternion.Euler(90, 0, 0); //x = 90 because prefab's original x-value of Rotation is 90.

                Rigidbody projectileRB = projectileGO.GetComponent<Rigidbody>();
                projectileRB.velocity = Vector3.zero;
                projectileRB.AddForce(transform.forward * projectileSpeed, ForceMode.Impulse);

                //If it is run on Enemy, then they have infinite ammo. Only decrease if Player.
                if (!infiniteAmmo)
                {
                    ammoCount -= 1;
                    ammoCountEvent.RaiseEvent(ammoCount);
                }

                yield return waitTime;
            }
            else
            {
                yield return null;
            }
        }
    }

    private void BulletDamageCalculator()
    {
        if (Random.value < (gunsList.GunStatsList()[currentGunIndex].CritChance / 100)) // Values between 0 & 1(inclusive).
        {
            damageToApply = gunsList.GunStatsList()[currentGunIndex].CritDamage;
        }
        else
        {
            damageToApply = gunsList.GunStatsList()[currentGunIndex].Damage;
        }
        projectileGO.GetComponent<BulletBehaviour>().Damage = damageToApply;
    }
}
