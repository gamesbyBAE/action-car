using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarData : MonoBehaviour
{
    //[SerializeField] CarStats carStats = default;
    [Header("Stats")]
    [SerializeField] int mass = default;
    [SerializeField] int maxHP = default;
    [SerializeField] int maxSpeed = default;
    [SerializeField] int carControl = default;
    [SerializeField] float motorForce = default;
    [SerializeField] float reverseForce = default;
    [SerializeField] float breakForce = default;
    [SerializeField] float maxSteerAngle = default;

    [Space]
    [SerializeField] BoxCollider boxCollider = default;
    [SerializeField] Transform gunMountPosition = default;

    [Header("Wheel Colliders")]
    [SerializeField] WheelCollider frontLeftWC = default;
    [SerializeField] WheelCollider frontRightWC = default;
    [SerializeField] WheelCollider backLeftWC = default;
    [SerializeField] WheelCollider backRightWC = default;

    [Header("Wheel Transforms")]
    [SerializeField] Transform frontLeftWheel = default;
    [SerializeField] Transform frontRightWheel = default;
    [SerializeField] Transform backLeftWheel = default;
    [SerializeField] Transform backRightWheel = default;

    // Getters
    public int Mass => mass;
    public int CarMaxHP => maxHP;
    public int MaxSpeed => maxSpeed;
    public int CarControl => carControl;
    public float MotorForce => motorForce;
    public float ReverseForce => reverseForce;
    public float BreakForce => breakForce;
    public float MaxSteerAngle => maxSteerAngle;

    public BoxCollider CarCollider
    {
        set
        {
            boxCollider = value;
        }
        get
        {
            return boxCollider;
        }
    }

    public Vector3 GunMountPosition => gunMountPosition.transform.position;

    #region Wheel Colliders
    public WheelCollider FrontLeftWC => frontLeftWC;
    public WheelCollider FrontRightWC => frontRightWC;
    public WheelCollider BackLeftWC => backLeftWC;
    public WheelCollider BackRightWC => backRightWC;
    #endregion

    #region Wheel Transforms
    public Transform FrontLeftWheel => frontLeftWheel;
    public Transform FrontRightWheel => frontRightWheel;
    public Transform BackLeftWheel => backLeftWheel;
    public Transform BackRightWheel => backRightWheel;
    #endregion

    //public CarStats CarStats => carStats;
}
