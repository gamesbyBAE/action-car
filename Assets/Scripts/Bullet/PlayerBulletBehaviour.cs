using UnityEngine;

public class PlayerBulletBehaviour : BulletBehaviour
{
    [Space]
    [SerializeField] StringVariableSO playerTag = default;

    public override void OnCollisionEnter(Collision collision)
    {
        //To ignore the collision with the Player collider.
        //'Physics.IgnoreCollision' can also be used but then first,
        //Player has to be found to get it's collider.
        if (collision.gameObject.CompareTag(playerTag.StringValue()))
        {
            return;
        }
        base.OnCollisionEnter(collision);
    }
}
