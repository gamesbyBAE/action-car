using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    [Tooltip("How long the bullet stays active before it's deactivated.")]
    [SerializeField] float lifeTime = default;

    [SerializeField] StringVariableSO targetTag = default;

    [Header("Events to Raise")]
    [SerializeField] GameObjectEventChannelSO usedBullet = default; //To put back it in the pool.
    [SerializeField] Vector3EventChannelSO pointOfContactEvent = default;
    public int Damage { get; set; }
    private void OnEnable()
    {
        Invoke("DisableBullet", lifeTime);
    }
    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(targetTag.StringValue()))
        {
            collision.gameObject.GetComponent<HP>().ChangeHP(-Damage);
            pointOfContactEvent.RaiseEvent(this.gameObject.transform.position);
        }
        DisableBullet();
    }
    void DisableBullet()
    {
        usedBullet.RaiseEvent(this.gameObject);
    }
}
