using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Tracks player when in range and raises an Int event with it's InstanceID as argument
/// for EnemyFire class to know it is raised by it's detector(child).
/// </summary>
public class TrackPlayer : MonoBehaviour
{
    [SerializeField] StringVariableSO playerTag = default;

    bool trackPlayer;
    GameObject playerGO;
    EnemyShoot enemyShoot;

    private void Start()
    {
        trackPlayer = false;
        playerGO = null;
        enemyShoot = GetComponentInChildren<EnemyShoot>();
    }

    private void Update()
    {
        if (trackPlayer)
        {
            this.transform.LookAt(playerGO.transform);
        }    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag.StringValue()))
        {
            if (playerGO == null)
            {
                playerGO = other.gameObject;
            }

            trackPlayer = true;
            enemyShoot.StartFiring = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(playerTag.StringValue()))
        {
            trackPlayer = false;
            enemyShoot.StartFiring = false;
        }
    }
}
