using UnityEngine;

/// <summary>
/// Keeps a track of how many turrets have been destroyed.
/// Raises a game over event if all the turrets have been destroyed.
/// </summary>
public class DeathCounter : MonoBehaviour
{
    [SerializeField] GameObject turretsHolder = default;

    [Header("Events to Subscribe")]
    [SerializeField] VoidEventChannelSO onTurretDeath = default;
    [SerializeField] VoidEventChannelSO killCountRequestedEvent = default;

    [Header("Events to Raise")]
    [SerializeField] VoidEventChannelSO gameOverEvent = default;
    [SerializeField] IntEventChannelSO killCountValueEvent = default;
    [SerializeField] IntEventChannelSO totalTurretCountValueEvent = default;

    int deathCount, totalTurrets;
    public int DeathCount => deathCount;
    void Start()
    {
        deathCount = 0;
        totalTurrets = turretsHolder.transform.childCount;
    }

    private void OnEnable()
    {
        onTurretDeath.OnVoidEventRaised += IncreaseCount;
        killCountRequestedEvent.OnVoidEventRaised += SendKillCount;
    }
    private void OnDisable()
    {
        onTurretDeath.OnVoidEventRaised -= IncreaseCount;
        killCountRequestedEvent.OnVoidEventRaised -= SendKillCount;
    }
    private void IncreaseCount()
    {
        deathCount += 1;

        if (deathCount == totalTurrets)
        {
            gameOverEvent.RaiseEvent();
        }
    }

    void SendKillCount()
    {
        killCountValueEvent.RaiseEvent(deathCount);
        totalTurretCountValueEvent.RaiseEvent(totalTurrets);
    }
}
