using UnityEngine;

/// <summary>
/// Shows/Hides the Time Options menu.
/// </summary>
public class ShowTimeOptionsMenu : MonoBehaviour
{
    [SerializeField] GameObject timeOptionsGO = default;

    [Header("Events to Subscribe")]
    [SerializeField] BoolEventChannelSO showMenu = default;

    private void OnEnable()
    {
        showMenu.OnBoolValueRaised += ShowMenu;
    }
    private void OnDisable()
    {
        showMenu.OnBoolValueRaised -= ShowMenu;
    }
    void ShowMenu(bool val)
    {
        if (val)
        {
            timeOptionsGO.SetActive(true);
        }
        else
        {
            timeOptionsGO.SetActive(false);
        }
    }
}
