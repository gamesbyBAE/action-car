using UnityEngine;
using UnityEngine.UI;

public class CarStatsVisualiser : MonoBehaviour
{
    [SerializeField] GOListVariableSO carsList = default;

    [Header("Upper Bounds")]
    [SerializeField] int maxHPValue = default;
    [SerializeField] int maxSpeedValue = default;
    [SerializeField] int maxControlValue = default;

    [Header("Fill Images")]
    [SerializeField] Image hpFillImage = default;
    [SerializeField] Image speedFillImage = default;
    [SerializeField] Image controlFillImage = default;

    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO carIndexEvent = default;

    private void OnEnable()
    {
        carIndexEvent.OnIntValueRaised += UpdateInfo;
    }
    private void OnDisable()
    {
        carIndexEvent.OnIntValueRaised -= UpdateInfo;
    }
    private void Start()
    {
        UpdateInfo(UserSelection.Instance.CarIndex);
    }
    private void UpdateInfo(int val)
    {
        CarData currentCar = carsList.GameObjectList()[val].GetComponent<CarData>();

        hpFillImage.fillAmount = (float)currentCar.CarMaxHP / maxHPValue;
        speedFillImage.fillAmount = (float)currentCar.MaxSpeed / maxSpeedValue;
        controlFillImage.fillAmount = (float)currentCar.CarControl / maxControlValue;
    }
}
