using UnityEngine;

/// <summary>
/// Displays either Gun Stats or Car Stats depending on the buttons
/// pressed by the user.
/// </summary>
public class StatsSwitcher : MonoBehaviour
{
    [SerializeField] GameObject carStats = default;
    [SerializeField] GameObject gunStats = default;

    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO carIndexEvent = default;
    [SerializeField] IntEventChannelSO gun1IndexEvent = default;
    [SerializeField] IntEventChannelSO gun2IndexEvent = default;

    private void OnEnable()
    {
        carIndexEvent.OnIntValueRaised += ShowCarStats;
        gun1IndexEvent.OnIntValueRaised += ShowGunStats;
        gun2IndexEvent.OnIntValueRaised += ShowGunStats;

        ShowCarStats(0);
    }
    private void OnDisable()
    {
        carIndexEvent.OnIntValueRaised -= ShowCarStats;
        gun1IndexEvent.OnIntValueRaised -= ShowGunStats;
        gun2IndexEvent.OnIntValueRaised -= ShowGunStats;
    }
    void ShowCarStats(int val)
    {
        carStats.SetActive(true);
        gunStats.SetActive(false);
    }
    void ShowGunStats(int val)
    {
        carStats.SetActive(false);
        gunStats.SetActive(true);
    }
}
