using UnityEngine;
using UnityEngine.UI;

public class GunStatsVisualiser : MonoBehaviour
{
    [SerializeField] GunStatsListVariableSO gunsList = default;

    [Header("Upper Bounds")]
    [SerializeField] int maxAmmoValue = default;
    [SerializeField] int maxFireRateValue = default;
    [SerializeField] int maxDamageValue = default;
    [SerializeField] int maxCritRateValue = default;

    [Header("Fill Images")]
    [SerializeField] Image ammoFillImage = default;
    [SerializeField] Image fireRateFillImage = default;
    [SerializeField] Image damageFillImage = default;
    [SerializeField] Image critRateFillImage = default;

    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO gun1IndexEvent = default;
    [SerializeField] IntEventChannelSO gun2IndexEvent = default;

    private void OnEnable()
    {
        gun1IndexEvent.OnIntValueRaised += UpdateInfo;
        gun2IndexEvent.OnIntValueRaised += UpdateInfo;

        UpdateInfo(UserSelection.Instance.Gun1Index);
    }
    private void OnDisable()
    {
        gun1IndexEvent.OnIntValueRaised -= UpdateInfo;
        gun2IndexEvent.OnIntValueRaised -= UpdateInfo;
    }
    void UpdateInfo(int val)
    {
        ammoFillImage.fillAmount = (float)gunsList.GunStatsList()[val].MaxAmmo / maxAmmoValue;
        fireRateFillImage.fillAmount = (float)gunsList.GunStatsList()[val].FireRate / maxFireRateValue;
        damageFillImage.fillAmount = (float)gunsList.GunStatsList()[val].Damage / maxDamageValue;
        critRateFillImage.fillAmount = (float)gunsList.GunStatsList()[val].CritChance / maxCritRateValue;
    }
}
