using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewSpawner : MonoBehaviour
{
    [SerializeField] GOListVariableSO carsList = default;
    [SerializeField] GunStatsListVariableSO gunsList = default;

    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO carIndexEvent = default;
    [SerializeField] IntEventChannelSO gun1IndexEvent = default;
    [SerializeField] IntEventChannelSO gun2IndexEvent = default;

    GameObject[] tempCarArray, tempGunArray;

    int prevCarIndex, prevGunIndex;

    private void OnEnable()
    {
        carIndexEvent.OnIntValueRaised += SpawnCar;
        gun1IndexEvent.OnIntValueRaised += SpawnGun;
        gun2IndexEvent.OnIntValueRaised += SpawnGun;
    }
    private void OnDisable()
    {
        carIndexEvent.OnIntValueRaised -= SpawnCar;
        gun1IndexEvent.OnIntValueRaised -= SpawnGun;
        gun2IndexEvent.OnIntValueRaised -= SpawnGun;
    }

    private void Start()
    {
        tempCarArray = new GameObject[3];
        for (int i = 0; i < carsList.GameObjectList().Count; i++)
        {
            GameObject go = Instantiate(carsList.GameObjectList()[i], transform);
            go.SetActive(false);
            tempCarArray[i] = go;
        }

        tempGunArray = new GameObject[3];
        for (int i = 0; i < gunsList.GunStatsList().Count; i++)
        {
            GameObject go = Instantiate(gunsList.GunStatsList()[i].GunPrefab, transform);
            Vector3 gunMountPosition = tempCarArray[UserSelection.Instance.CarIndex].transform.GetChild(0).transform.position;
            go.transform.position =  gunMountPosition;
            go.SetActive(false);
            tempGunArray[i] = go;
        }



        prevCarIndex = 0;
        prevGunIndex = 0;

        SpawnCar(0);
        SpawnGun(0);
    }

    void SpawnCar(int val)
    {
        tempCarArray[prevCarIndex].SetActive(false);
        tempCarArray[val].SetActive(true);
        prevCarIndex = val;

        RepositionGun();
    }
    void SpawnGun(int val)
    {
        tempGunArray[prevGunIndex].SetActive(false);
        tempGunArray[val].SetActive(true);
        prevGunIndex = val;

        RepositionGun();
    }
    void RepositionGun()
    {
        GameObject go = tempGunArray[prevGunIndex];
        go.transform.position = tempCarArray[prevCarIndex].transform.GetChild(0).transform.position;
    }
}
