using UnityEngine;

/// <summary>
/// Stores user selections made in "Selection Scene".
/// </summary>
public class UserSelection : GenericSingletonClass<UserSelection>
{
    [Header("Events to Subscribe")]
    [SerializeField] IntEventChannelSO carIndexEvent = default;
    [SerializeField] IntEventChannelSO gun1IndexEvent = default;
    [SerializeField] IntEventChannelSO gun2IndexEvent = default;
    [SerializeField] IntEventChannelSO timeLimitValueEvent = default;

    int carIndex;
    public int CarIndex => carIndex;

    int gun1Index; 
    public int Gun1Index => gun1Index;

    int gun2Index;
    public int Gun2Index => gun2Index;

    int timeLimit;
    public int TimeLimit => timeLimit;

    private void OnEnable()
    {
        carIndexEvent.OnIntValueRaised += SetCarIndex;
        gun1IndexEvent.OnIntValueRaised += SetGun1Index;
        gun2IndexEvent.OnIntValueRaised += SetGun2Index;
        timeLimitValueEvent.OnIntValueRaised += SetTimeLimit;
    }
    private void OnDisable()
    {
        carIndexEvent.OnIntValueRaised -= SetCarIndex;
        gun1IndexEvent.OnIntValueRaised -= SetGun1Index;
        gun2IndexEvent.OnIntValueRaised -= SetGun2Index;
        timeLimitValueEvent.OnIntValueRaised -= SetTimeLimit;
    }
    private void Start()
    {
        SetCarIndex(0);
        SetGun1Index(0);
        SetGun2Index(0);
        SetTimeLimit(30);
    }
    private void SetCarIndex(int arg0)
    {
        carIndex = arg0;
    }
    private void SetGun1Index(int arg0)
    {
        gun1Index = arg0;
    }
    private void SetGun2Index(int arg0)
    {
        gun2Index = arg0;
    }
    public void SetTimeLimit(int val)
    {
        timeLimit = val;
    }
}
