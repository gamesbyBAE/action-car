using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pools game object, takes out from as well as
/// puts them back in pool.
/// </summary>
public class Pooler : MonoBehaviour
{
    [SerializeField] GameObject poolingPrefab = default;
    [SerializeField] int poolAmount = default;

    [Header("Events to Subscribe")]
    [SerializeField] GameObjectEventChannelSO usedGO = default;

    Queue<GameObject> poolQueue;

    private void OnEnable()
    {
        usedGO.OnGameObjectValueRaised += PutInPool;
    }
    private void OnDisable()
    {
        usedGO.OnGameObjectValueRaised -= PutInPool;
    }

    private void Awake()
    {
        poolQueue = new Queue<GameObject>();
        for (int i = 0; i < poolAmount; i++)
        {
            GameObject go = Instantiate(poolingPrefab, transform);
            go.SetActive(false);
            poolQueue.Enqueue(go);
        }
    }

    public GameObject GetFromPool()
    {
        // If there's something in the queue, then get that object else instantiate.
        GameObject go = poolQueue.Count > 0 ? poolQueue.Dequeue() : Instantiate(poolingPrefab, transform);
        go.SetActive(true);
        return go;
    }

    void PutInPool(GameObject go)
    {
        go.SetActive(false);
        poolQueue.Enqueue(go);
    }
}
